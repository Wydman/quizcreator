package QuestionTypes;

import java.util.ArrayList;
import java.util.List;

public class QCM extends Question {
    private List<String> answers = new ArrayList<String>();
    int trueAnswerIndex;

    public QCM(int id,String question, String trueAnswer, List<String> falseAnswers) {
        super(id,question, trueAnswer, 1);
        answers.add(trueAnswer);
        answers.addAll(falseAnswers);

        for (String s : answers)
            if (s.equals(answerText)) {
                trueAnswerIndex = answers.indexOf(s);
            }
    }

    public boolean checkAnswer(String userAnswer) {
        return(userAnswer.equals(Integer.toString(trueAnswerIndex)) || userAnswer.equals(answerText));
    }
}



