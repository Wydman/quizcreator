package com.MainPackage.Factory;

import QuestionTypes.Question;
import com.MainPackage.Repository.QuestionRepository;
import com.MainPackage.Service.QuestionIdGenerator;
import org.springframework.stereotype.Component;

@Component
public class CreateSimpleQuestion {
    QuestionRepository questionRepository;
    QuestionIdGenerator questionIdGenerator;

    public CreateSimpleQuestion(QuestionRepository questionRepo, QuestionIdGenerator idGenerat) {
        questionRepository = questionRepo;
        questionIdGenerator = idGenerat;
    }

    public Question createNewSimpleQuestion(String questionText,String questionAnswer) {
        return new Question(questionIdGenerator.generaterQuestionId(),questionText,questionAnswer,1);
    }
}
