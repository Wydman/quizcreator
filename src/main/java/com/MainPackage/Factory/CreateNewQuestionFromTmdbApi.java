package com.MainPackage.Factory;

import QuestionTypes.Question;
import com.MainPackage.Service.QuestionIdGenerator;
import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Random;

@Component
public class CreateNewQuestionFromTmdbApi {
    QuestionIdGenerator questionIdGenerator;

    public CreateNewQuestionFromTmdbApi(QuestionIdGenerator idGen){
        questionIdGenerator = idGen;
    }

    public Optional<Question> GetNewQuestionFromTmdbApi(String MovieName, int difficulty, TmdbClient tmdbClient) {
        Optional<MovieInfo> optionalMovieInfo = tmdbClient.getMovie(MovieName);
        return setRandomStyleQuestions(optionalMovieInfo, difficulty);
    }

    public Optional<Question> GetNewQuestionFromTmdbApi(int difficulty, TmdbClient tmdbClient) {
        Optional<MovieInfo> optionalMovieInfo = tmdbClient.getRandomPopularMovie();
        return setRandomStyleQuestions(optionalMovieInfo, difficulty);
    }

    private Optional<Question> setRandomStyleQuestions(Optional<MovieInfo> optionalMovieInfo, int i) {
        switch (i) {
            case 0:
                return optionalMovieInfo.map(movieInfo -> createOptionalQuestionDiff1(movieInfo));
            case 1:
                return optionalMovieInfo.map(movieInfo -> createOptionalQuestionDiff2(movieInfo));
            case 2:
                return optionalMovieInfo.map(movieInfo -> createOptionalQuestionDiff3(movieInfo));
            default:
                System.out.println("ce niveau de difficulté n'éxiste pas");
                return Optional.empty();
        }
    }

    private Question createOptionalQuestionDiff1(MovieInfo movieInfo) {

        String nameAssemblage = "";

        for (int i = 0; i < movieInfo.cast.size() && i < 3; i++) {
            nameAssemblage += " - " + movieInfo.cast.get(i).actorName;
        }

        Question question = new Question(questionIdGenerator.generaterQuestionId(), "Quel film sorti en " + movieInfo.year.toString() +
                " contient des personnages joués par les acteurs suivants : " + nameAssemblage, movieInfo.title, 2);
        return question;
    }

    public Question createOptionalQuestionDiff2(MovieInfo movieInfo) {
        int randomIndex = new Random().nextInt(movieInfo.cast.size());
        Question question = new Question(questionIdGenerator.generaterQuestionId(), "Quel acteur connu à joué le rôle de : " + movieInfo.cast.get(randomIndex).characterName +
                " dans le film  : " + movieInfo.title, movieInfo.cast.get(randomIndex).actorName, 5);
        return question;
    }

    public Question createOptionalQuestionDiff3(MovieInfo movieInfo) {
        int randomIndex = new Random().nextInt(movieInfo.cast.size());
        Question question = new Question(questionIdGenerator.generaterQuestionId(), "Comment s'appelle le personnage de  " + movieInfo.cast.get(randomIndex).actorName +
                " dans le film " + movieInfo.title, movieInfo.cast.get(randomIndex).actorName, 7);
        return question;
    }
}
