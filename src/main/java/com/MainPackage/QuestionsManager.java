package com.MainPackage;

import QuestionTypes.Question;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class QuestionsManager {
    Scanner sc = new Scanner(System.in);

    public boolean AskAllTypesOfQuestions(Question question) {
        System.out.println(question.getQuestionText() + " ID :" + question.getQuestionID());
        return question.checkAnswer(sc.nextLine());
    }
}
