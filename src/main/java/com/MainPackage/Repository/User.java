package com.MainPackage.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class User {
    private int userID;
    private String userName;
    private String userPassword;
    private int userBestScore;
    private HashMap<Date, Integer> previousScoreList = new HashMap<>();

    public User(int userID, String userName, String userPassword) {

        this.userID = userID;
        this.userName = userName;
        this.userPassword = userPassword;
    }

    public User() {

    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public int getUserBestScore() {
        return userBestScore;
    }

    public void setUserBestScore(int userBestScore) {
        this.userBestScore += userBestScore;
    }

    public HashMap<Date, Integer> getPreviousScoreList() {
        return previousScoreList;
    }

    public void setPreviousScoreList(HashMap<Date, Integer> previousScoreList) {
        this.previousScoreList = previousScoreList;
    }
}
