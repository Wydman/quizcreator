package com.MainPackage.Repository;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Component
public class UserRepository {
    private HashMap<Integer, User> userMap = new HashMap<>();


    public User newUser(User user) {
        userMap.put(user.getUserID(), user);
        System.out.println("User : " + user.getUserName() + " with ID : " + user.getUserID() + " Saved");
        return user;
    }

    public User getUser(int id) {
        return userMap.get(id);
    }

    public int getUserIDByName(String name) {
        AtomicReference<Integer> index = new AtomicReference<>();
        index.set(0);
        userMap.forEach((key, value) -> {
            if (value.getUserName().equals(name)) {
                index.set(key);
            }
        });
       if(!index.get().equals(0)) return index.get();
       else return 0;
    }

    public boolean tryConnect(String userName, String password) {
        AtomicReference<User> user = new AtomicReference<>();
        user.set(new User());
        userMap.forEach((key, value) -> {
            if (value.getUserName().equals(userName)) {
                user.set(value);
            }
        });
        return user.get().getUserPassword().equals(password);
    }

    public List<User> getAllUsers() {
        List<User> list = new ArrayList<User>();
        userMap.forEach((key, value) -> list.add(value));
        return list;
    }

    public void deleteUser(int id) {
        userMap.remove(id);
    }

    public void deleteAllUsers() {
        userMap.clear();
    }
}
