package com.MainPackage;

import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.Scanner;

@Configuration
public class BasicBeansCreator {
    @Bean
    public CommandLineRunner commandLineRunner(CoreProgramManager coreProgramManager, FilesManager filesManager, Scanner sc, TmdbClient tmdbClient) {
        return args -> {
            coreProgramManager.start(sc,tmdbClient);
            filesManager.initializeFiles();
        };
    }

    @Bean
    public Scanner createScanner() {
        return new Scanner(System.in);
    }

    @Bean
    public TmdbClient tmdbBeanClient(@Value("classpath:ApikeyFile") org.springframework.core.io.Resource resource) throws IOException {
        return new TmdbClient(resource.getFile());
    }
}


