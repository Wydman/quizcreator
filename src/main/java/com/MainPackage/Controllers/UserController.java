package com.MainPackage.Controllers;

import com.MainPackage.Repository.User;
import com.MainPackage.Repository.UserRepository;
import com.MainPackage.Service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {


    private UserRepository userRepository;
    private UserService userService;

    public UserController(UserRepository userRepository, UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }


    @GetMapping("/{id}")
    public UserRepresentation getUser(@PathVariable(value = "id") int id) {
        return new UserRepresentation(id, userService.getUserByID(id).getUserName(),
                userService.getUserByID(id).getUserBestScore(),
                userService.getUserByID(id).getPreviousScoreList());
    }

    @PostMapping("/searchUser")
    public boolean isUserRegistered(@RequestBody UserRepresentation userRepresentation) {
        if (userService.isUserRegistered(userRepresentation.getUserName()) != 0) {
            return true;
        } else {
            return false;
        }
    }

    @PostMapping("/addPoints")
    public UserRepresentation addPointsToUser(@RequestBody UserRepresentation userRepresentation) {
       User user = userService.addPoints(userRepresentation.getUserID(),userRepresentation.getPointsToAdd());
        return new UserRepresentation(user.getUserID()
                ,user.getUserName(),
                user.getUserBestScore());
    }

    @PostMapping("/getUserByName")
    public UserRepresentation getUserByName(@RequestBody UserRepresentation userRepresentation) {
        User user = userService.getUserByName(userRepresentation.getUserName());
        return new UserRepresentation(user.getUserID(), user.getUserName(), user.getUserBestScore(), user.getPreviousScoreList());
    }

    @PostMapping("/tryConnect")
    public boolean tryConnect(@RequestBody UserRepresentation userRepresentation) {
        return userRepository.tryConnect(userRepresentation.getUserName(), userRepresentation.getUserPassword());
    }

    @GetMapping("")
    public List<UserRepresentation> getAllUsers() {
        List<UserRepresentation> userRepresentationList = new ArrayList<>();
        for (User user : userRepository.getAllUsers()) {
            userRepresentationList.add(new UserRepresentation(user.getUserID(),
                    user.getUserName(),
                    user.getUserBestScore(),
                    user.getPreviousScoreList()));
        }
        return userRepresentationList;
    }


    @PostMapping("/create")
    public UserRepresentation createUser(@RequestBody UserRepresentation userRepresentation) {
        User user = userService.createUser(userRepresentation.getUserName(), userRepresentation.getUserPassword());
        return (new UserRepresentation(user.getUserID(), user.getUserName(), user.getUserBestScore(), user.getPreviousScoreList()));
    }

}
