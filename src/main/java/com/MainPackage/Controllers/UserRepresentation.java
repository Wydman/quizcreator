package com.MainPackage.Controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRepresentation {
    private int userID;
    private String userName;
    private String userPassword;
    private int userBestScore;
    private int pointsToAdd;
    private Map<Date, Integer> previousScoreList = new HashMap<Date, Integer>();

    UserRepresentation(int userID, String userName, String userPassword, int userBestScore, HashMap<Date, Integer> previousScoreList) {

        this.userID = userID;
        this.userName = userName;
        this.userPassword = userPassword;
        this.userBestScore = userBestScore;
        this.previousScoreList = previousScoreList;
    }

    UserRepresentation(int userID, String userName, int userBestScore, HashMap<Date, Integer> previousScoreList) {

        this.userID = userID;
        this.userName = userName;
        this.userBestScore = userBestScore;
        this.previousScoreList = previousScoreList;
    }

    UserRepresentation(String userName,String userPassword) {
        this.userName = userName;
        this.userPassword = userPassword;
    }

    UserRepresentation(int userID,int userBestScore) {
        this.userID = userID;
        this.userBestScore = userBestScore;
    }

    UserRepresentation() {

    }

    UserRepresentation(int userID, String userName, int userBestScore) {

        this.userID = userID;
        this.userName = userName;
        this.userBestScore = userBestScore;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public int getUserBestScore() {
        return userBestScore;
    }

    public void setUserBestScore(int userBestScore) {
        this.userBestScore = userBestScore;
    }

    public Map<Date, Integer> getPreviousScoreList() {
        return previousScoreList;
    }

    public void setPreviousScoreList(Map<Date, Integer> previousScoreList) {
        this.previousScoreList = previousScoreList;
    }

    public int getPointsToAdd() {
        return pointsToAdd;
    }

    public void setPointsToAdd(int pointsToAdd) {
        this.pointsToAdd = pointsToAdd;
    }
}
