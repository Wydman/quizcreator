package com.MainPackage.Controllers;

import QuestionTypes.Question;
import com.MainPackage.Factory.CreateNewQuestionFromTmdbApi;
import com.MainPackage.Repository.QuestionRepository;
import com.MainPackage.Service.CreateQuestion;
import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/questions")
public class QuestionController {
    private QuestionRepository questionRepository;
    private CreateNewQuestionFromTmdbApi createNewQuestionFromTmdbApi;
    private TmdbClient tmdbClient;
    private CreateQuestion createQuestion;

    public QuestionController(QuestionRepository questionRepo, CreateNewQuestionFromTmdbApi createNewQuestion, TmdbClient tmdb, CreateQuestion createQ) {
        questionRepository = questionRepo;
        createNewQuestionFromTmdbApi = createNewQuestion;
        tmdbClient = tmdb;
        createQuestion = createQ;
    }

    @GetMapping("/{id}")
    public Question requestQuestion(@PathVariable(value = "id") int id) {
        return questionRepository.getQuestion(id);
    }

    @PostMapping("{id}/tryAnswer")
    public AnswerRepresentation tryAnwser(@PathVariable(value = "id") int id, @RequestBody AnswerRepresentation answerRepresentation) {
        return new AnswerRepresentation(id, answerRepresentation.getAnswer(), questionRepository.getQuestion(id).checkAnswer(answerRepresentation.getAnswer()));
    }

    @GetMapping("")
    public List<QuestionRepresentation> requestQuestion() {
        List<QuestionRepresentation> questionRepresentationList = new ArrayList<>();
        for (Question question : questionRepository.getAllQuestions()) {
            questionRepresentationList.add(new QuestionRepresentation(question.getQuestionID(),
                    question.getQuestionText(),
                    question.getAnswerText()));
        }
        return questionRepresentationList;
    }

    @PostMapping("/simplequestion")
    public QuestionRepresentation createQuestion(@RequestBody QuestionRepresentation questionRepresentation) {
        return createQuestion.createNewSimpleQuestion(questionRepresentation.getQuestionText(), questionRepresentation.getQuestionAnswer());
    }

    @PostMapping("")
    public QuestionRepresentation createRandomQuestion(@RequestBody QuestionRepresentation questionRepresentation) {
        return createQuestion.createQuestion(questionRepresentation.getMovieName());
    }
}

