package com.MainPackage.Controllers;

public class QuestionRepresentation {

    private int id;
    private String displayableText;
    private String questionText;
    private String questionAnswer;
    private String movieName;

    public QuestionRepresentation(int idNumber, String questionT, String questionA) {
        id = idNumber;
        questionAnswer = questionA;
        questionText = questionT;
        movieName = "";
    }

    public QuestionRepresentation(int idNumber, String displayText) {
        displayableText = displayText;
        id = idNumber;
        movieName = "";
    }

    public QuestionRepresentation(String movieN) {
        movieName = movieN;
    }

    public QuestionRepresentation() {
        id = 0;
        displayableText = "";
        movieName = "";
    }

    public int getId() {
        return id;
    }

    public String getDisplayableText() {
        return displayableText;
    }

    public String getQuestionAnswer() {
        return questionAnswer;
    }

    public String getQuestionText() {
        return questionText;
    }

    public String getMovieName() {
        return movieName;
    }
}
