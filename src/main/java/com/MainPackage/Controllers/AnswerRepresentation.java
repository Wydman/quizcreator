package com.MainPackage.Controllers;

public class AnswerRepresentation {
    private int id;
    private String answer;
    private Boolean answerResult;

    public AnswerRepresentation(int id, String aswr, Boolean answerResult) {
        this.id = id;
        this.answer = aswr;
        this.answerResult = answerResult;
    }

    public AnswerRepresentation(int id, String aswr) {
        this.id = id;
        this.answer = aswr;
    }

    public AnswerRepresentation() {
        answer = "";
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String aswr) {
        answer = aswr;
    }

    public Boolean getAnswerResult() {
        return answerResult;
    }

    public void setAnswerResult(Boolean answerResult) {
        this.answerResult = answerResult;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
