package com.MainPackage.Service;

import org.springframework.stereotype.Component;

@Component
public class UserIdGenerator {
    private int generatedUserId = 0;

    public int generaterUserId() {
        return generatedUserId++;
    }
}