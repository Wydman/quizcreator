package com.MainPackage.Service;

import QuestionTypes.Question;
import com.MainPackage.Controllers.QuestionRepresentation;
import com.MainPackage.Factory.CreateNewQuestionFromTmdbApi;
import com.MainPackage.Factory.CreateSimpleQuestion;
import com.MainPackage.Repository.QuestionRepository;
import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class CreateQuestion {
    private String questionText;
    private String answerText;
    private int questionID;
    private CreateNewQuestionFromTmdbApi createNewQuestionFromTmdbApi;
    private TmdbClient tmdbClient;
    private CreateSimpleQuestion createSimpleQuestion;
    private QuestionRepository questionRepository;

    public CreateQuestion(CreateNewQuestionFromTmdbApi createNewQuestion, TmdbClient tmdb, QuestionRepository questionRepo, CreateSimpleQuestion createNewSimpleQ) {
        createNewQuestionFromTmdbApi = createNewQuestion;
        tmdbClient = tmdb;
        questionRepository = questionRepo;
        createSimpleQuestion = createNewSimpleQ;
    }

    public QuestionRepresentation createQuestion(String movieName) {
        if (movieName.equals("")) {
            createNewQuestionFromTmdbApi.GetNewQuestionFromTmdbApi(new Random().nextInt(3), tmdbClient).ifPresent(questionq -> {
                questionID = questionq.getQuestionID();
                questionRepository.save(questionq);
            });
            return new QuestionRepresentation(questionRepository.getQuestion(questionID).getQuestionID(),
                    questionRepository.getQuestion(questionID).getQuestionText(),
                    questionRepository.getQuestion(questionID).getAnswerText());
        } else {
            createNewQuestionFromTmdbApi.GetNewQuestionFromTmdbApi(movieName, new Random().nextInt(3), tmdbClient).ifPresent(questionq -> {
                questionID = questionq.getQuestionID();
                questionRepository.save(questionq);
            });
            return new QuestionRepresentation(questionRepository.getQuestion(questionID).getQuestionID(),
                    questionRepository.getQuestion(questionID).getQuestionText(),
                    questionRepository.getQuestion(questionID).getAnswerText());
        }
    }

    public QuestionRepresentation createNewSimpleQuestion(String questionText, String answerText) {
        Question question = createSimpleQuestion.createNewSimpleQuestion(questionText, answerText);
        questionRepository.save(question);
        return new QuestionRepresentation(question.getQuestionID(),questionText,answerText);
    }

    public String getAnswerText() {
        return answerText;
    }

    public String getQuestionText() {
        return questionText;
    }

    public int getQuestionID() {
        return questionID;
    }
}
