package com.MainPackage.Service;

import com.MainPackage.Repository.User;
import com.MainPackage.Repository.UserRepository;
import org.springframework.stereotype.Component;

@Component
public class UserService {

    private UserIdGenerator userIdGenerator;
    private UserRepository userRepository;

    public UserService(UserIdGenerator userIdGenerator, UserRepository userRepository) {

        this.userIdGenerator = userIdGenerator;
        this.userRepository = userRepository;
    }

    public User createUser(String userName, String userPassword) {
        boolean alreadyRegister = false;
        for (User user : userRepository.getAllUsers()) {
            if(user.getUserName().equals(userName)){
                alreadyRegister = true;
                break;
            }
        }

        if (!alreadyRegister)
            return userRepository.newUser(new User(userIdGenerator.generaterUserId(), userName, userPassword));
        else{
            return new User();
        }
    }

    public User getUserByID(int id) {
        return userRepository.getUser(id);
    }

    public User addPoints(int id,int points) {
        userRepository.getUser(id).setUserBestScore(points);
        return userRepository.getUser(id);
    }

    public int isUserRegistered(String name) {
        return userRepository.getUserIDByName(name);
    }
    public User getUserByName(String name) {
        return getUserByID(userRepository.getUserIDByName(name));
    }
}
